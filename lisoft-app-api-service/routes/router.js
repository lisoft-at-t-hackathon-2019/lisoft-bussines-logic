const db = require("../database/firebase");

module.exports = app => {
  app.get("/api/remote", (req, res) => {
    let query = req.query.type;
    switch (query) {
      case "record":
        db.getCollection("temperatureData", result => {
          res.json(result);
        });
        break;

      case "stock":
        db.getCollection("stock", result => {
          res.json(result);
        });
        break;

      case "storage":
        db.getCollection("storageDevices", result => {
          res.json(result);
        });
        break;

      default:
        res.json({ status: "hah not working" });
        break;
    }
  });
};

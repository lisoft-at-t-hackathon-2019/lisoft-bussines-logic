# Mobile app api service

NodeJS service for requesting via mobile app.

# Routes

### /api/remote

Route is waiting for GET requests with no specific queries - record, stock, storage.

# Author

Radek Kucera | **radakuceru@gmail.com**

const restify = require("restify");
const corsMiddleware = require("restify-cors-middleware");

const cors = corsMiddleware({
  origins: ["*"]
});

const port = 3003;

const server = restify.createServer();
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.pre(cors.preflight);
server.use(cors.actual);

require("./routes/router")(server);

server.listen(port, () => console.log(`Service is listening on port ${port}!`));

const admin = require("firebase-admin");
const serviceAccount = require("../configs/serviceAccountKey");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://lisoft-data.firebaseio.com"
});

const db = admin.firestore();

// Class representing database connection
class Firebase {
  /**
   * @param  {} collection
   * @param  {} callback
   */
  static getCollection(collection, callback) {
    db.collection(collection)
      .get()
      .then(res => {
        let array = [];
        res.forEach(item => {
          array.push(item.data());
        });
        callback(array);
      });
  }
}

module.exports = Firebase;

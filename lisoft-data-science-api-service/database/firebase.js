const admin = require("firebase-admin");
const serviceAccount = require("../configs/serviceAccountKey");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://lisoft-data.firebaseio.com"
});

const db = admin.firestore();

// Class representing database connection
class Firebase {
  /**
   * @param  {} callback
   */
  static getHistory(callback) {
    this.getStorages(storages => {
      let storageArray = [];
      storages.forEach(storage => {
        db.collection("Records")
          .where("eid", "==", storage.EID)
          .get()
          .then(res => {
            let array = [];
            res.forEach(item => {
              array.push({
                [item.data().datetime._seconds]: {
                  product_key: item.data().p_key,
                  amount: item.data().amount
                }
              });
            });
            storageArray.push({
              eid: storage.EID,
              data: array
            });
          });
      });
      setTimeout(() => {
        callback({ storages: storageArray });
      }, 2000);
    });
  }
  /**
   * @param  {} callback
   */
  static getStorages(callback) {
    db.collection("storageDevices")
      .get()
      .then(res => {
        let array = [];
        res.forEach(item => {
          array.push(item.data());
        });
        callback(array);
      });
  }
}

module.exports = Firebase;

const admin = require("firebase-admin");
const serviceAccount = require("../configs/serviceAccountKey");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://lisoft-data.firebaseio.com"
});

const db = admin.firestore();

// Class representing database connection
class Firebase {
  /**
   * @param  {} callback
   */
  static getFreezers(callback) {
    db.collection("storageDevices")
      .get()
      .then(res => {
        let array = [];
        res.forEach(item => {
          array.push(item.data());
        });
        callback(array);
      });
  }
  /**
   * @param  {} callback
   */
  static getDevices(callback) {
    this.getFreezers(res => {
      this.getArray(res, data => {
        callback(data);
      });
    });
  }
  /**
   * @param  {} res
   * @param  {} callback
   */
  static getArray(res, callback) {
    let array = [];
    for (let i = 0; i < res.length; i++) {
      let item = res[i];
      this.searchRecord(item, result => {
        array.push(result);
      });
    }

    // SHHHHHHHHHHH!!!!! TOHLE NIKDO NEVIDÍ .( Its hackathon right?
    setTimeout(() => {
      callback(array);
    }, 2000);
  }
  /**
   * @param  {} item
   * @param  {} callback
   */
  static searchRecord(item, callback) {
    this.getRecordByEID(item, result => {
      let obj = {
        ...item,
        history: result
      };
      callback(obj);
    });
  }
  /**
   * @param  {} freezer
   * @param  {} callback
   */
  static getRecordByEID(freezer, callback) {
    db.collection("temperatureData")
      .where("EID", "==", freezer.EID)
      .get()
      .then(res => {
        let array = [];
        res.forEach(item => {
          let tempdata = item.data();
          let obj = {
            EID: freezer.EID,
            Name: freezer.Name,
            Temp: tempdata.Temp,
            Time: tempdata.Timestamp._seconds
          };
          array.push(obj);
        });
        let filteredArray = array.sort(compare).splice(0, 9);
        callback(filteredArray);
      });
  }
}

function compare(a, b) {
  const TempA = a.Time;
  const TempB = b.Time;

  let comparison = 0;
  if (TempA < TempB) {
    comparison = 1;
  } else if (TempA > TempB) {
    comparison = -1;
  }
  return comparison;
}

module.exports = Firebase;

# Client api service

NodeJS service for requesting via client app.

# Routes

### /api/communication

Route is waiting for GET requests with no specific queries - record, stock, storage.

# Author

Radek Kucera | **radakuceru@gmail.com**
